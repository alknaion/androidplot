package com.androidplot.gesture;


public interface OnPlotDoubleTapListener {
	public boolean onDoubleTap();
}
