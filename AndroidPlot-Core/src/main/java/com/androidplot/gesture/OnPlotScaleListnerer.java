package com.androidplot.gesture;

public interface OnPlotScaleListnerer {
	public boolean onScale(float previousDistance, float currentDistance);
}
