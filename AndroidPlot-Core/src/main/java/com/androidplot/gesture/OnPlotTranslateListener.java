package com.androidplot.gesture;

import android.graphics.PointF;

public interface OnPlotTranslateListener {
	public boolean onTranslate(PointF previousPosition, PointF currentPosition);
}
