package com.androidplot.gesture;

import android.content.Context;
import android.graphics.PointF;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class PlotGestureDetector extends GestureDetector.SimpleOnGestureListener implements OnTouchListener {
	// Definition of the touch states
    enum State
    {
        NONE,
        ONE_FINGER_DRAG,
        TWO_FINGERS_DRAG,
        TWO_FINGERS_DRAG_INVALID,
        THREE_OR_MORE_FINGERS_DRAG
    }

   
	private static final float MIN_DIST_2_FING = 5f;
	private final GestureDetector mGestureDetector;
	private final OnPlotGestureListener mOnPlotGestureListener;
	private PointF mOriginalPosition;

	private float mInitialDistance;
	private State mFingerState = State.NONE;
	
	@Override
    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public boolean onDoubleTap(MotionEvent motionEvent) {
    	return mOnPlotGestureListener.onDoubleTap();
    }

	public PlotGestureDetector(Context context, OnPlotGestureListener onPlotGestureListener) {
		mOnPlotGestureListener = onPlotGestureListener;
		mGestureDetector = new GestureDetector(context, this);
	}
	
	private float getDistance(final MotionEvent event) {
        return (float)Math.sqrt(Math.pow(event.getX(0) - event.getX(1), 2) + Math.pow(event.getY(0) - event.getY(1), 2));
    }

	private boolean initialize(MotionEvent motionEvent) {
		switch(motionEvent.getPointerCount()) {
		case 0:
			mFingerState = State.NONE;
			return true;
		case 1:
			mOriginalPosition = new PointF(motionEvent.getX(), motionEvent.getY());
			mFingerState = State.ONE_FINGER_DRAG;
			return true;
		case 2: 
			mInitialDistance = getDistance(motionEvent);
			mFingerState = mInitialDistance > MIN_DIST_2_FING ? State.TWO_FINGERS_DRAG: State.TWO_FINGERS_DRAG_INVALID;
			return true;
		default:
			mFingerState = State.THREE_OR_MORE_FINGERS_DRAG;
			return false;
		}
	}
	
	private boolean use(MotionEvent motionEvent) {
		switch(mFingerState) {
		case ONE_FINGER_DRAG:
			if(1 == motionEvent.getPointerCount()) {
				PointF newPosition = new PointF(motionEvent.getX(), motionEvent.getY());
				if(mOnPlotGestureListener.onTranslate(mOriginalPosition, newPosition)) { 
					mOriginalPosition = newPosition;
					return true;
				}
			} else {
				return initialize(motionEvent);
			}
			break;
		case TWO_FINGERS_DRAG: 
			if(2 == motionEvent.getPointerCount()) {
				float newDistance = getDistance(motionEvent);
				if(mOnPlotGestureListener.onScale(mInitialDistance, newDistance)) {
					mInitialDistance = newDistance;
					return true;
				}
			} else {
				return initialize(motionEvent);
			}
			break;
		case TWO_FINGERS_DRAG_INVALID:
			return initialize(motionEvent);
		default:
			break;
		}
		return false;
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent motionEvent) {
		boolean consumed = mGestureDetector.onTouchEvent(motionEvent);
		
		switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_DOWN:
		case MotionEvent.ACTION_UP:
		case MotionEvent.ACTION_POINTER_DOWN: 
		case MotionEvent.ACTION_POINTER_UP:
			consumed |= initialize(motionEvent);
			break;
			
		case MotionEvent.ACTION_MOVE:
			consumed |= use(motionEvent);
			break;
		}
		return consumed;
	}
}