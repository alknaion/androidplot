package com.androidplot.xy;

import android.content.Context;
import android.graphics.PointF;
import android.util.AttributeSet;

import com.androidplot.gesture.OnPlotDoubleTapListener;
import com.androidplot.gesture.OnPlotGestureListener;
import com.androidplot.gesture.PlotGestureDetector;

public class XYPlotTransform extends XYPlot implements OnPlotGestureListener {

	private float mMinDomainLowerBoundaryLimit = Float.MAX_VALUE;
	private float mMaxDomainUpperBoundaryLimit = Float.MAX_VALUE;
	private float mMinRangeLowerBoundaryLimit = Float.MAX_VALUE;
	private float mMaxRangeUpperBoundaryLimit = Float.MAX_VALUE;
	private float mLastMinDomainLowerBoundary = Float.MAX_VALUE;
	private float mLastMaxDomainUpperBoundary = Float.MAX_VALUE;
	private float mLastMinRangeLowerBoundary = Float.MAX_VALUE;
	private float mLastMaxRangeUpperBoundary = Float.MAX_VALUE;

	private boolean mIsTransformEnabled;
	private boolean mIsRangeTransformEnabled;
	private boolean mIsDomainTransformEnabled;
	private boolean mIsGestureEnabled;
	private OnPlotGestureListener mOnPlotGestureListener;
	private OnPlotDoubleTapListener mOnPlotDoubleTapListener;
	private final PlotGestureDetector mPlotGestureDetector;
	

	public XYPlotTransform(final Context context, final String title, final RenderMode mode) {
		super(context, title, mode);
		mPlotGestureDetector = new PlotGestureDetector(context, this);
	}
	
	public XYPlotTransform(final Context context, final String title) {
		super(context, title);
		mPlotGestureDetector = new PlotGestureDetector(context, this);
	
	}
	
	public XYPlotTransform(final Context context, final AttributeSet attrs) {
		super(context, attrs);
		mPlotGestureDetector = new PlotGestureDetector(context, this);
		setTransformEnabled(true);
		mIsDomainTransformEnabled = true;
		mIsRangeTransformEnabled = true;
	}

	public XYPlotTransform(final Context context, final AttributeSet attrs, final int defStyle) {
		super(context, attrs, defStyle);
		mPlotGestureDetector = new PlotGestureDetector(context, this);
		setTransformEnabled(true);
		mIsDomainTransformEnabled = true;
		mIsRangeTransformEnabled = true;
	}

	

	public void setOnPlotGestureListener(OnPlotGestureListener onPlotGestureListener) {
		mOnPlotGestureListener = onPlotGestureListener;
	}
	
	public void setOnPlotDoubleTapListener(OnPlotDoubleTapListener onPlotDoubleTapListener) {
		mOnPlotDoubleTapListener = onPlotDoubleTapListener;
	}
	
	
	@Override
	public void setOnTouchListener(OnTouchListener l) {
		if (l != this) {
			mIsTransformEnabled = false;
		}
		super.setOnTouchListener(l);
	}

	public boolean isRangeTransformEnabled() {
		return mIsRangeTransformEnabled;
	}

	public void setRangeTransformEnabled(boolean isRangeTransformEnabled) {
		if(isRangeTransformEnabled && !mIsTransformEnabled) {
			setTransformEnabled(true);
		}
		mIsRangeTransformEnabled = isRangeTransformEnabled;
	}

	public boolean isDomainTransformEnabled() {
		return mIsDomainTransformEnabled;
	}

	public void setDomainTransformEnabled(boolean isDomainTransformEnabled) {
		if(isDomainTransformEnabled && !mIsTransformEnabled) {
			setTransformEnabled(true);
		}
		mIsDomainTransformEnabled = isDomainTransformEnabled;
	}

	public void setTransformEnabled(boolean isTransformEnabled) {
		if (isTransformEnabled && !mIsGestureEnabled) {
			setGesture(true);
		}
		mIsTransformEnabled = isTransformEnabled;
	}

	public boolean isTransformEnabled() {
		return mIsTransformEnabled;
	}

	public void setGesture(boolean isGestureEnabled) {
		if(mIsGestureEnabled == isGestureEnabled) {
			return;
		}
		if(isGestureEnabled) {
			setOnTouchListener(mPlotGestureDetector);
		} else {
			setOnTouchListener(null);
		}
		mIsGestureEnabled = isGestureEnabled;
	}
	
	private float getMinDomainLowerBoundaryLimit() {
		if (mMinDomainLowerBoundaryLimit == Float.MAX_VALUE) {
			mMinDomainLowerBoundaryLimit = getCalculatedMinX().floatValue();
			mLastMinDomainLowerBoundary = mMinDomainLowerBoundaryLimit;
		}
		return mMinDomainLowerBoundaryLimit;
	}

	private float getMaxDomainUpperBoundaryLimit() {
		if (mMaxDomainUpperBoundaryLimit == Float.MAX_VALUE) {
			mMaxDomainUpperBoundaryLimit = getCalculatedMaxX().floatValue();
			mLastMaxDomainUpperBoundary = mMaxDomainUpperBoundaryLimit;
		}
		return mMaxDomainUpperBoundaryLimit;
	}

	private float getMinRangeLowerBoundaryLimit() {
		if (mMinRangeLowerBoundaryLimit == Float.MAX_VALUE) {
			mMinRangeLowerBoundaryLimit = getCalculatedMinY().floatValue();
			mLastMinRangeLowerBoundary = mMinRangeLowerBoundaryLimit;
		}
		return mMinRangeLowerBoundaryLimit;
	}

	private float getMaxRangeUpperBoundaryLimit() {
		if (mMaxRangeUpperBoundaryLimit == Float.MAX_VALUE) {
			mMaxRangeUpperBoundaryLimit = getCalculatedMaxY().floatValue();
			mLastMaxRangeUpperBoundary = mMaxRangeUpperBoundaryLimit;
		}
		return mMaxRangeUpperBoundaryLimit;
	}

	private float getLastMinDomainLowerBoundary() {
		if (mLastMinDomainLowerBoundary == Float.MAX_VALUE) {
			mLastMinDomainLowerBoundary = getCalculatedMinX().floatValue();
		}
		return mLastMinDomainLowerBoundary;
	}

	private float getLastMaxDomainUpperBoundary() {
		if (mLastMaxDomainUpperBoundary == Float.MAX_VALUE) {
			mLastMaxDomainUpperBoundary = getCalculatedMaxX().floatValue();
		}
		return mLastMaxDomainUpperBoundary;
	}

	private float getLastMinRangeLowerBoundary() {
		if (mLastMinRangeLowerBoundary == Float.MAX_VALUE) {
			mLastMinRangeLowerBoundary = getCalculatedMinY().floatValue();
		}
		return mLastMinRangeLowerBoundary;
	}

	private float getLastMaxRangeUpperBoundary() {
		if (mLastMaxRangeUpperBoundary == Float.MAX_VALUE) {
			mLastMaxRangeUpperBoundary = getCalculatedMaxY().floatValue();
		}
		return mLastMaxRangeUpperBoundary;
	}

	@Override
	public boolean onDoubleTap() {
		boolean isProcessed = false;
		if(null != mOnPlotGestureListener) {
			isProcessed |= mOnPlotGestureListener.onDoubleTap(); 
		}
		if(null != mOnPlotDoubleTapListener) {
			isProcessed |= mOnPlotDoubleTapListener.onDoubleTap();
		}
		return isProcessed;
	}

	@Override
	public boolean onTranslate(PointF previousPosition, PointF currentPosition) {
		return translate(previousPosition, currentPosition);
	}

	@Override
	public boolean onScale(float previousDistance, float currentDistance) {
		return scale(previousDistance, currentDistance);

	}

	@Override
	protected void setDomainUpperBoundaryInternal(Number boundary, BoundaryMode mode) {
		super.setDomainUpperBoundaryInternal(boundary, mode);
		mMaxDomainUpperBoundaryLimit = mode == BoundaryMode.FIXED ? boundary.floatValue() : getCalculatedMaxX().floatValue();
		mLastMaxDomainUpperBoundary = mMaxDomainUpperBoundaryLimit;
	}

	@Override
	protected void setDomainLowerBoundaryInternal(Number boundary, BoundaryMode mode) {
		super.setDomainLowerBoundaryInternal(boundary, mode);
		mMinDomainLowerBoundaryLimit = mode == BoundaryMode.FIXED ? boundary.floatValue() : getCalculatedMinX().floatValue();
		mLastMinDomainLowerBoundary = mMinDomainLowerBoundaryLimit;
	}

	@Override
	protected void setRangeUpperBoundaryInternal(Number boundary, BoundaryMode mode) {
		super.setRangeUpperBoundaryInternal(boundary, mode);
		mMaxRangeUpperBoundaryLimit = mode == BoundaryMode.FIXED ? boundary.floatValue() : getCalculatedMaxY().floatValue();
		mLastMaxRangeUpperBoundary = mMaxRangeUpperBoundaryLimit;

	}

	@Override
	protected void setRangeLowerBoundaryInternal(Number boundary, BoundaryMode mode) {
		super.setRangeLowerBoundaryInternal(boundary, mode);
		mMinRangeLowerBoundaryLimit = mode == BoundaryMode.FIXED ? boundary.floatValue() : getCalculatedMinY().floatValue();
		mLastMinRangeLowerBoundary = mMinRangeLowerBoundaryLimit;
	}

	private boolean translate(PointF previousPosition, PointF currentPosition) {
		if(!mIsDomainTransformEnabled && !mIsRangeTransformEnabled) {
			return false;
		}
		Boundary<Float> newBoundary = new Boundary<Float>();
		if (mIsDomainTransformEnabled) {
			calculateTranslateDomainBoundary(previousPosition, currentPosition, newBoundary);
			
			super.setDomainLowerBoundaryInternal(newBoundary.getLower(), BoundaryMode.FIXED);
			super.setDomainUpperBoundaryInternal(newBoundary.getUpper(), BoundaryMode.FIXED);
			
			mLastMinDomainLowerBoundary = newBoundary.getLower();
			mLastMaxDomainUpperBoundary = newBoundary.getUpper();
		}
		if (mIsRangeTransformEnabled) {
			calculateTranslateRangeBoundary(previousPosition, currentPosition, newBoundary);
			
			super.setRangeLowerBoundaryInternal(newBoundary.getLower(), BoundaryMode.FIXED);
			super.setRangeUpperBoundaryInternal(newBoundary.getUpper(), BoundaryMode.FIXED);
			
			mLastMinRangeLowerBoundary = newBoundary.getLower();
			mLastMaxRangeUpperBoundary = newBoundary.getUpper();
		}
		redraw();
		return true;
	}

	
	
	private void calculateTranslateDomainBoundary(final PointF oldPosition, final PointF newPosition, Boundary<Float> newBoundary) {
        // multiply the absolute finger translatement for a factor.
        // the factor is dependent on the calculated min and max
		float domainLowerBoundary = getLastMinDomainLowerBoundary();
		float domainUpperBoundary = getLastMaxDomainUpperBoundary();
		final float offset = (oldPosition.x - newPosition.x) * ((domainUpperBoundary - domainLowerBoundary) / getWidth());
        
        // translate the calculated offset
        domainLowerBoundary += offset;
        domainUpperBoundary += offset;
        
        //get the distance between max and min
        final float distance = domainUpperBoundary - domainLowerBoundary;
		
        
        //check if we reached the limit of panning
        if(domainLowerBoundary < getMinDomainLowerBoundaryLimit()) {
            domainLowerBoundary = getMinDomainLowerBoundaryLimit();
            domainUpperBoundary = domainLowerBoundary + distance;
        }
        if(domainUpperBoundary > getMaxDomainUpperBoundaryLimit()) {
            domainUpperBoundary = getMaxDomainUpperBoundaryLimit();
            domainLowerBoundary = domainUpperBoundary - distance;
        }
        
        newBoundary.setLower(domainLowerBoundary);
        newBoundary.setUpper(domainUpperBoundary);
    }

	
	
	private void calculateTranslateRangeBoundary(final PointF oldPosition, final PointF newPosition, Boundary<Float> newBoundary) {
        // multiply the absolute finger translatement for a factor.
        // the factor is dependent on the calculated min and max
		float rangeLowerBoundary = getLastMinRangeLowerBoundary();
		float rangeUpperBoundary = getLastMaxRangeUpperBoundary();
        final float offset = -(oldPosition.y - newPosition.y) * ((rangeUpperBoundary - rangeLowerBoundary) / getHeight());
		
        // translate the calculated offset
        rangeLowerBoundary += offset;
        rangeUpperBoundary += offset;
        
        //get the distance between max and min
        final float distance = rangeUpperBoundary - rangeLowerBoundary;
		
        
        //check if we reached the limit of panning
        if(rangeLowerBoundary < getMinRangeLowerBoundaryLimit()) {
            rangeLowerBoundary = getMinRangeLowerBoundaryLimit();
            rangeUpperBoundary = rangeLowerBoundary + distance;
        }
        if(rangeUpperBoundary > getMaxRangeUpperBoundaryLimit()) {
            rangeUpperBoundary = getMaxRangeUpperBoundaryLimit();
            rangeLowerBoundary = rangeUpperBoundary - distance;
        }
        
        newBoundary.setLower(rangeLowerBoundary);
        newBoundary.setUpper(rangeUpperBoundary);
    }

	class Boundary<T extends Object> {
		private T mUpper;
		private T mLower;
		public Boundary() {
			mLower = null;
			mUpper = null;
		}
		public Boundary(T lower, T upper) {
			mLower = lower;
			mUpper = upper;
		}
		
		void setUpper(T upper) {
			mUpper = upper;
		}
		
		void setLower(T lower) {
			mLower = lower;
		}
		T getUpper() { 
			return mUpper;
		}
		
		T getLower()  { 
			return mLower;
		}
	}
	
	private boolean scale(float previousDistance, float currentDistance) {
		if(!mIsDomainTransformEnabled && !mIsRangeTransformEnabled) {
			return false;
		}
		
		float scale = (previousDistance / currentDistance);
		// sanity check
		if (Float.isInfinite(scale) || Float.isNaN(scale) || scale > -0.001 && scale < 0.001) {
			return true;
		}
		Boundary<Float> newBoundary = new Boundary<Float>();
		
		if (mIsDomainTransformEnabled) {
			calculateScaleDomainBoundary(scale, newBoundary);
			
			super.setDomainLowerBoundaryInternal(newBoundary.getLower(), BoundaryMode.FIXED);
			super.setDomainUpperBoundaryInternal(newBoundary.getUpper(), BoundaryMode.FIXED);
			
			mLastMinDomainLowerBoundary = newBoundary.getLower();
			mLastMaxDomainUpperBoundary = newBoundary.getUpper();
		}
		
		if (mIsRangeTransformEnabled) {
			calculateScaleRangeBoundary(scale, newBoundary, false);

			super.setRangeLowerBoundaryInternal(newBoundary.getLower(), BoundaryMode.FIXED);
			super.setRangeUpperBoundaryInternal(newBoundary.getUpper(), BoundaryMode.FIXED);
			
			mLastMinRangeLowerBoundary = newBoundary.getLower();
			mLastMaxRangeUpperBoundary = newBoundary.getUpper();
		}
		
		redraw();
		return true;
	}

	private void calculateScaleDomainBoundary(float scale, Boundary<Float> newBoundary) {
		final float lastMaxDomainUpperBoundary = getLastMaxDomainUpperBoundary();
		final float distance = lastMaxDomainUpperBoundary - getLastMinDomainLowerBoundary();
		final float midPoint = lastMaxDomainUpperBoundary - (distance / 2.0f);
		final float offset = distance * scale / 2.0f;
		
		newBoundary.setLower(midPoint - offset);
		newBoundary.setUpper(midPoint + offset);
		
		if (newBoundary.getLower() < getMinDomainLowerBoundaryLimit()) {
			newBoundary.setLower(getMinDomainLowerBoundaryLimit());
		}
		if (newBoundary.getUpper() > getMaxDomainUpperBoundaryLimit()) {
			newBoundary.setUpper(getMaxDomainUpperBoundaryLimit());
		}
	}
	
	private void calculateScaleRangeBoundary(float scale, Boundary<Float> newBoundary, final boolean horizontal) {
		final float lastMaxRangeUpperBoundary = getLastMaxRangeUpperBoundary();
		final float distance = lastMaxRangeUpperBoundary - getLastMinRangeLowerBoundary();
		final float midPoint = lastMaxRangeUpperBoundary - (distance / 2.0f);
		final float offset = distance * scale / 2.0f;
		
		newBoundary.setLower(midPoint - offset);
		newBoundary.setUpper(midPoint + offset);
	
		if (newBoundary.getLower() < getMinRangeLowerBoundaryLimit()) {
			newBoundary.setLower(getMinRangeLowerBoundaryLimit());
		}
		if (newBoundary.getUpper() > getMaxRangeUpperBoundaryLimit()) {
			newBoundary.setUpper(getMaxRangeUpperBoundaryLimit());
		}
	}
}
